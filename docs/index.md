---
title: Documentation
redirect_from:
  - /wiki/
  - /wiki/index.php/
  - /core-mods/
---

Index
-----

[Terminology](/docs/terminology/)

<!-- Arrow symbol: → -->

### Minecraft 1.12
* [Redstone Flux](/docs/redstone-flux/)
* [CoFH Core 4.6](/docs/cofh-core/) - <span class="uk-text-small uk-text-success">up to date (4.6.1)</span>
* [CoFH World](/docs/cofh-world/) - <span class="uk-text-small uk-text-warning">WIP (1.1.1 → 1.3.0)</span>
* [Thermal Foundation 2.6](/docs/thermal-foundation/) - <span class="uk-text-small uk-text-success">up to date (2.6.1)</span>
* [Thermal Expansion 5.5](/docs/thermal-expansion/) - <span class="uk-text-small uk-text-success">up to date (5.5.2)</span>
* [Thermal Dynamics 2.5](/docs/thermal-dynamics/) - <span class="uk-text-small uk-text-success">up to date (2.5.3)</span>
* [Thermal Cultivation](/docs/thermal-cultivation/) - <span class="uk-text-small uk-text-success">up to date (0.3.1)</span>
* [Thermal Innovation](/docs/thermal-innovation/) - <span class="uk-text-small uk-text-success">up to date (0.3.1)</span>
* [Redstone Arsenal 2.6](/docs/redstone-arsenal/) - <span class="uk-text-small uk-text-success">up to date (2.6.1)</span>
* [Vanilla+ Tools](/docs/vanillaplus-tools/) - <span class="uk-text-small uk-text-success">up to date (1.1.0)</span>
* [Vanilla+ Satchels](/docs/vanillaplus-satchels/) - <span class="uk-text-small uk-text-success">up to date (1.0.1)</span>

<!--
### Minecraft 1.11
* [CoFH Core 4.2](/docs/cofh-core/4.2/)
* [Thermal Foundation 2.2](/docs/thermal-foundation/2.2/)
* [Thermal Expansion 5.2](/docs/thermal-expansion/5.2/)
* [Thermal Dynamics 2.2](/docs/thermal-dynamics/2.2/)
* [Redstone Arsenal 2.2](/docs/redstone-arsenal/2.2/)

### Minecraft 1.10
* [CoFH Core 4.1](/docs/cofh-core/4.1/)
* [Thermal Foundation 2.1](/docs/thermal-foundation/2.1/)
* [Thermal Expansion 5.1](/docs/thermal-expansion/5.1/)
* [Thermal Dynamics 2.0](/docs/thermal-dynamics/2.0/)
* [Redstone Arsenal 2.1](/docs/redstone-arsenal/2.1/)
* [MineFactory Reloaded 2.9](/docs/minefactory-reloaded/2.9/)

### Minecraft 1.7
* [CoFH Core 3](/docs/cofh-core/3/)
* [Thermal Foundation 1](/docs/thermal-foundation/1/)
* [Thermal Expansion 4](/docs/thermal-expansion/4/)
* [Thermal Dynamics 1](/docs/thermal-dynamics/1/)
* [Redstone Arsenal 1.1](/docs/redstone-arsenal/1.1/)
* [MineFactory Reloaded 2.8](/docs/minefactory-reloaded/2.8/)
* [Nether Ores 2.3](/docs/nether-ores/2.3/)

### Minecraft 1.6
* [CoFH Core 2](/docs/cofh-core/2/)
* [Thermal Expansion 3](/docs/thermal-expansion/3/)
* [Redstone Arsenal 1.0](/docs/redstone-arsenal/1.0/)
* [MineFactory Reloaded 2.7](/docs/minefactory-reloaded/2.7/)
* [Nether Ores 2.2](/docs/nether-ores/2.2/)
-->
